// OCaret.h: interface for the OCaret class.
// 作者：司徒汇编民间科学工作室，共和国山东青岛
//////////////////////////////////////////////////////////////////////

#if !defined(__OCARET_H__)
#define __OCARET_H__

typedef struct _CARET
{
	BOOL	bValid;				// 是否使用脱字符
	int	x;						// 位置
	int	y;
	int	w;						// 宽高
	int	h;
	BOOL	bFlash;				// 是否闪烁
	BOOL	bShow;				// 标志当前状态为显示或者隐藏
	ULONGLONG	lTimeInterval;	// 闪烁的时间间隔（一般采用500毫秒）
} CARET;

class OCaret
{
public:
	CARET m_Caret;
	ULONGLONG m_lLastTime;

public:
	OCaret();
	virtual ~OCaret();

	// 根据窗口的脱字符信息设置系统脱字符的参数；
	BOOL SetCaret (CARET* pCaret);

	// 更新脱字符的显示。
	// 如果脱字符定时器到时了，则将主缓中脱字符区域的图像以适当方式送入FrameBuffer的对应位置；
	BOOL Check (LCD* pLCD, LCD* pBuf);

	// 绘制脱字符
	void OCaret::DrawCaret (LCD* pLCD, LCD* pBuf);
};

#endif // !defined(__OCARET_H__)
