// stdafx.h : include file for standard system include files,
//  or project specific include files that are used frequently, but
//      are changed infrequently
//

#if !defined(AFX_STDAFX_H__86AE138D_F50C_4711_B297_536BC5F7C5E3__INCLUDED_)
#define AFX_STDAFX_H__86AE138D_F50C_4711_B297_536BC5F7C5E3__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#define VC_EXTRALEAN		// Exclude rarely-used stuff from Windows headers

#include <afxwin.h>         // MFC core and standard components
#include <afxext.h>         // MFC extensions
#include <afxdisp.h>        // MFC Automation classes
#include <afxdtctl.h>		// MFC support for Internet Explorer 4 Common Controls
#ifndef _AFX_NO_AFXCMN_SUPPORT
#include <afxcmn.h>			// MFC support for Windows Common Controls
#endif // _AFX_NO_AFXCMN_SUPPORT

// 定义显示区域的尺寸
#define CLIENT_W	830
#define CLIENT_H	590
#define SCREEN_W	480
#define SCREEN_H	320
#define SCREEN_LEFT 20
#define SCREEN_TOP  20

// 定义最大控件数目
#define CTRL_MAX	60

// 定义对话框题头的高度
#define TITLE_HEIGHT 15

// 定义窗口类型
#define	WND_TYPE_DIALOG			101
#define	WND_TYPE_STATIC			102
#define	WND_TYPE_BUTTON			103
#define	WND_TYPE_EDIT			104
#define	WND_TYPE_LIST			105
#define	WND_TYPE_COMBO			106
#define	WND_TYPE_PROGRESS		107
#define WND_TYPE_IMAGE_BUTTON   108
#define WND_TYPE_CHECK_BOX      109

// 定义控件风格
#define WND_STYLE_NORMAL        0x0
#define WND_STYLE_NO_BORDER     0x0001		// 无边框(用于按钮和对话框之外的控件)
#define WND_STYLE_ROUND_EDGE    0x0002		// 圆角(只用于对话框和少数控件)
#define WND_STYLE_PASSWORD      0x0004		// 密码(只用于编辑框)
#define WND_STYLE_GROUP         0x0008		// 组框(只用于静态文本)
#define WND_STYLE_SOLID         0x0010		// 立体效果(可用于任何控件，建议只用于对话框和按钮)
#define WND_STYLE_ORIDEFAULT    0x0020		// 原始默认按钮(只用于按钮)
#define WND_STYLE_NO_TITLE      0x0040		// 无题头(只用于对话框)
#define WND_STYLE_DISABLE_IME   0x0080		// 禁用输入法(只对编辑框和组合框有效)
#define WND_STYLE_NO_SCROLL     0x0100		// 无滚动条(只对列表框和组合框有效)
#define WND_STYLE_AUTO_DROPDOWN 0x0200		// 自动弹出下拉菜单(只对组合框有效)
#define WND_STYLE_AUTO_OPEN_IME 0x0400		// 自动打开拼音输入法
#define WND_STYLE_IGNORE_ENTER  0x0800      // EDIT控件不处理回车键

// 定义键盘宏名称与键值映射表的文件名
#define KEYMAP_FILENAME         "KEYMAP.txt"

// 定义用于对话框和控件的数据结构
typedef struct _CONTROL_INFO
{
	int nType;		// 对话框不存储此项
	int nStyle;
	int nX1;
	int nY1;
	int nX2;
	int nY2;
	int nID;
	BYTE psCaption[256];
	int nAddData;	// 对话框不存储此项

	int width()  { return nX2 - nX1; };
	int height() { return nY2 - nY1; };
}CONTROL_INFO;

// 定义全局函数
// 从一行文字中取得两个分号之间的片断
BOOL GetSlice( CString* pstrReturn, CString strInput, int k );

// 从一段文字中取出一行
BOOL GetLine( CString* pstrReturn, CString strInput, int k );

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_STDAFX_H__86AE138D_F50C_4711_B297_536BC5F7C5E3__INCLUDED_)
