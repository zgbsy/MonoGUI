// AttributeDlg.cpp : implementation file
//

#include "stdafx.h"
#include "DTEditor.h"
#include "AttributeDlg.h"

#include "Ctrl.h"
#include "CtrlMgt.h"
#include "EditorView.h"
#include "StyleDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CAttributeDlg dialog

CAttributeDlg::CAttributeDlg(CEditorView* pParent)
	: CDialog(CAttributeDlg::IDD, pParent)
{
	m_pView = pParent;
	//{{AFX_DATA_INIT(CAttributeDlg)
	//}}AFX_DATA_INIT
}

void CAttributeDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CAttributeDlg)
	DDX_Control(pDX, IDC_BUTTON, m_btnEditStyle);
	DDX_Control(pDX, IDC_EDIT, m_editValue);
	DDX_Control(pDX, IDC_LIST, m_lstAttribute);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CAttributeDlg, CDialog)
	//{{AFX_MSG_MAP(CAttributeDlg)
	ON_NOTIFY(NM_CLICK, IDC_LIST, OnClickList)
	ON_EN_KILLFOCUS(IDC_EDIT, OnKillfocusEdit)
	ON_BN_CLICKED(IDC_BUTTON, OnEditStyle)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CAttributeDlg message handlers

BOOL CAttributeDlg::Create() 
{
	BOOL bR = CDialog::Create (IDD_ATTRIBUTE);

	// 设置一行全部选中属性
	m_lstAttribute.SetExtendedStyle(LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);
	m_lstAttribute.InsertColumn(1, "属性", LVCFMT_LEFT, 75);
	m_lstAttribute.InsertColumn(2, "数值", LVCFMT_LEFT, 139);

	return bR;
}


void CAttributeDlg::OnCancel() 
{
}


void CAttributeDlg::SetText (CString strName, CString strValue)
{
	int iPos = m_lstAttribute.GetItemCount ();
	LV_ITEM lv;
	memset ((void *)&lv, 0, sizeof(LV_ITEM));
	lv.mask = LVIF_TEXT;
	lv.iItem = iPos;
	lv.iSubItem = 0;
	lv.pszText = (LPTSTR)(LPCTSTR)"EXISTUSER";
	m_lstAttribute.InsertItem(&lv);
	m_lstAttribute.SetItemText (iPos, 0, strName);
	m_lstAttribute.SetItemText (iPos, 1, strValue);
}


void CAttributeDlg::Renew( BOOL bEffect )
{
	// 数据有效
	if( bEffect )
	{
		// 根据m_pCtrl的属性初始化列表
		DiaplayAttribute();
		m_iSelectedIndex = -1;
	}
	else
	{
		// 清除列表的内容
		m_lstAttribute.DeleteAllItems();
		m_iSelectedIndex = -1;
	}
}


void CAttributeDlg::DiaplayAttribute()
{
	CCtrl* pCtrl;
	int nSel;
	if( !m_pView->m_pCtrlMgt->GetCurSel( &pCtrl, &nSel ) )
	{
		return;
	}

	// 清除列表的内容
	m_lstAttribute.DeleteAllItems();

	CString strName;
	CString strValue;

	strName = "控件类型";
	switch( pCtrl->m_ControlInfo.nType )
	{
	case WND_TYPE_DIALOG:
		strValue = "对话框";
		break;
	case WND_TYPE_STATIC:
		strValue = "静态文本";
		break;
	case WND_TYPE_BUTTON:
		strValue = "按钮";
		break;
	case WND_TYPE_EDIT:
		strValue = "编辑框";
		break;
	case WND_TYPE_LIST:
		strValue = "列表框";
		break;
	case WND_TYPE_COMBO:
		strValue = "组合框";
		break;
	case WND_TYPE_PROGRESS:
		strValue = "进度条";
		break;
	case WND_TYPE_IMAGE_BUTTON:
		strValue = "图像按钮";
		break;
	case WND_TYPE_CHECK_BOX:
		strValue = "复选框";
		break;
	default:{}
	}
	SetText(strName, strValue);

	strName = "控件风格";
	strValue.Format( "%d", pCtrl->m_ControlInfo.nStyle );
	SetText( strName, strValue );

	strName = "X坐标";
	strValue.Format( "%d", pCtrl->m_ControlInfo.nX1 );
	SetText( strName, strValue );

	strName = "Y坐标";
	strValue.Format( "%d", pCtrl->m_ControlInfo.nY1 );
	SetText( strName, strValue );

	strName = "宽度W";
	strValue.Format( "%d", pCtrl->m_ControlInfo.width() );
	SetText( strName, strValue );

	strName = "高度H";
	strValue.Format( "%d", pCtrl->m_ControlInfo.height() );
	SetText( strName, strValue );

	strName = "ID号";
	strValue.Format( "%d", pCtrl->m_ControlInfo.nID );
	SetText( strName, strValue );

	strName = "Caption";
	strValue.Format( "%s", pCtrl->m_ControlInfo.psCaption );
	SetText( strName, strValue );

	strName = "附加数据";
	strValue.Format( "%d", pCtrl->m_ControlInfo.nAddData );
	SetText( strName, strValue );
}

// CListCtrl 类 NM_CLICK 消息处理函数
void CAttributeDlg::OnClickList(NMHDR* pNMHDR, LRESULT* pResult) 
{
	// 得到选中的行数
	int iIndex = m_lstAttribute.GetSelectionMark();

	// 编辑选中的行
	if( iIndex > 0 )
	{
		CRect Rect;
		m_lstAttribute.GetItemRect (iIndex, &Rect, LVIR_BOUNDS);

		int iColumWidth = m_lstAttribute.GetColumnWidth (0);
		if (Rect.right > iColumWidth+4)
		{
			Rect.left = iColumWidth+3;
			Rect.top -= 2;
			Rect.bottom += 2;
		}

		if( iIndex > 1 )
		{
			// 初始化编辑框的内容，移动到选中位置，并显示
			m_editValue.SetWindowText (m_lstAttribute.GetItemText (iIndex, 1));
			m_editValue.MoveWindow (&Rect, TRUE);
			m_editValue.ShowWindow (SW_SHOW);
			m_bEditEnableState = TRUE;
			// （注：之所以添加这个标志是因为“失去焦点”有时会被调用多次）
			// 将选中的位置存入成员变量
			m_iSelectedIndex = iIndex;
		}

		// 如果是控件风格，则在编辑框的左边显示一个小按钮
		if( iIndex==1 )
		{
			Rect.left -= 22;
			Rect.right = Rect.left + 20;
			m_btnEditStyle.MoveWindow( &Rect, TRUE );
			m_btnEditStyle.ShowWindow( SW_SHOW );
		}
		else
		{
			m_btnEditStyle.ShowWindow( SW_HIDE );
		}
	}

	*pResult = 0;
}

//
void CAttributeDlg::OnKillfocusEdit() 
{
	// m_bEditEnableState标志保证失去焦点处理只进行一次
	if (!m_bEditEnableState)
	{
		return;
	}

	// 读出编辑框内容
	CString strValue;
	m_editValue.GetWindowText( strValue );

	// 将编辑框清空，设置为不显示状态
	m_editValue.SetWindowText ("");
	m_editValue.ShowWindow (SW_HIDE);

	// 写入数据库
	BOOL bSuccess = m_pView->m_pCtrlMgt->ModifyDataBase( m_iSelectedIndex, strValue );

	// 如果写入数据库成功（数值合法），则写入列表并刷新显示
	if (bSuccess)
	{
		m_lstAttribute.SetItemText (m_iSelectedIndex, 1, strValue);
		// 刷新视图的显示（针对位置参数的修改）
		m_pView->Display();
		// 设置“数据修改标志”
		m_pView->SetModified();
	}
	else
	{
		MessageBox ("数值非法，属性未能正确修改！", "提示信息", MB_OK | MB_ICONWARNING);
	}

	// 清除m_bEditEnableState标志
	m_bEditEnableState = FALSE;
}

// 编辑样式
void CAttributeDlg::OnEditStyle() 
{
	CCtrl* pCtrl;
	int iSel;
	if( !m_pView->m_pCtrlMgt->GetCurSel( &pCtrl, &iSel ) )
	{
		return;
	}

	// 首先关闭属性列表对消息的响应
	m_lstAttribute.EnableWindow( FALSE );
	m_editValue.EnableWindow( FALSE );
	m_btnEditStyle.EnableWindow( FALSE );

	// 如果是对话框，记录下对话框有无WND_STYLE_NO_TITLE风格
	BOOL bTitleStatus = FALSE;
	if( ( pCtrl->m_ControlInfo.nType == WND_TYPE_DIALOG ) && ( (pCtrl->m_ControlInfo.nStyle & WND_STYLE_NO_TITLE) == 0 ) )
	{
		bTitleStatus = TRUE;
	}

	// 打开风格选择对话框
	CStyleDlg dlg( pCtrl->m_ControlInfo.nType, pCtrl->m_ControlInfo.nStyle );
	if (dlg.DoModal() == IDOK)
	{
		// 修改数据库
		CString strValue;
		strValue.Format( "%d", dlg.m_nStyle );
		m_pView->m_pCtrlMgt->ModifyDataBase( 1, strValue );

		// 如果修改的是对话框的WND_STYLE_NO_TITLE风格，还要对所有控件的位置进行调整
		if( pCtrl->m_ControlInfo.nType == WND_TYPE_DIALOG )
		{
			// 如果修改的是对话框的WND_STYLE_NO_TITLE风格，
			// 应根据此风格的变化调整所有控件的位置
			BOOL bHaveTitle = ( (pCtrl->m_ControlInfo.nStyle & WND_STYLE_NO_TITLE) == 0 );
			if( bTitleStatus != bHaveTitle )
			{
				if( bHaveTitle )
				{
					m_pView->m_pCtrlMgt->AdjustTitle( TRUE );
				}
				else
				{
					m_pView->m_pCtrlMgt->AdjustTitle( FALSE );
				}
			}
		}

		// 修改属性列表的数值
		Renew( TRUE );
	}

	// 恢复属性列表对消息的响应
	m_lstAttribute.EnableWindow( TRUE );
	m_editValue.EnableWindow( TRUE );
	m_btnEditStyle.EnableWindow( TRUE );

	// 刷新显示
	m_pView->Display();
}

/* END */

BOOL CAttributeDlg::PreTranslateMessage(MSG* pMsg) 
{
	if((pMsg->message == WM_KEYDOWN) && (pMsg->wParam == VK_RETURN))
		return TRUE;
	
	return CDialog::PreTranslateMessage(pMsg);
}
